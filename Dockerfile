FROM bitnami/minideb:stretch

COPY install.sh /install.sh
COPY repl_manager.sh /repl_manager.sh
COPY setup.sh /setup.sh
RUN /install.sh

COPY chaperone.conf /etc/chaperone.d/chaperone.conf

EXPOSE 3306

ENTRYPOINT ["/usr/local/bin/chaperone"]
