#!/bin/sh

set -e

# Add the debian.incal.net repo.
install_packages curl gnupg
echo 'deb http://debian.incal.net/debian common/' \
     > /etc/apt/sources.list.d/incal.list
curl -s http://debian.incal.net/repo.key \
    | apt-key add -

apt-get -q update

# Package setup.
install_packages \
    mariadb-server \
    python3-pip python3-setuptools python3-wheel \
    masterelection
pip3 install chaperone
rm -fr /root/.cache/pip
mkdir /etc/chaperone.d

# Wipe data directory.
rm -fr /var/lib/mysql

# Cleanup packages used only for install.
apt-get --purge -y remove curl gnupg
apt-get -y autoremove

exit 0

