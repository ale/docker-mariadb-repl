#!/bin/sh

MASTERELECTION_NAME=${MASTERELECTION_NAME:-mariadb}
REPL_PASSWORD=${REPL_PASSWORD:-mariadb-replication}

if [ -z "${PUBLIC_ADDR}" ]; then
    echo "ERROR: PUBLIC_ADDR is undefined" >&2
    exit 2
fi

wait_for_mysql() {
    echo "Waiting for MySQL server to start up..." >&2
    while ! mysqladmin ping >/dev/null 2>&1; do
	sleep 1
    done
}

log() {
    echo " [*] $*" >&2
}

become_master() {
    log "Becoming master"
    mysql -NBe "STOP SLAVE; RESET MASTER"
}

become_slave() {
    local master_addr="$1"
    local master_port="${master_addr##*:}"
    local master_host="${master_addr%%:*}"

    log "Switching master to ${master_addr}"
    
    mysql -NBe "CHANGE MASTER TO MASTER_HOST='${master_host}', MASTER_PORT=${master_port}, MASTER_USER='replication', MASTER_PASSWORD='${REPL_PASSWORD}', MASTER_CONNECT_RETRY=10, MASTER_USE_GTID=current_pos; START SLAVE"
    # TODO: Wait for replication to catch up.
}

become_unknown() {
    log "Stopping slave (no master)"    
    mysql -NBe "STOP SLAVE;"
}

run_masterelection() {
    local args
    args="--name=${MASTERELECTION_NAME} --service-addr=${PUBLIC_ADDR}"
    if [ -n "${ETCD_SRV}" ]; then
	args="${args} --etcd-srv=${ETCD_SRV}"
    fi
    if [ -n "${ETCD_URLS}" ]; then
	args="${args} --etcd-urls=${ETCD_URLS}"
    fi
    if [ -n "${ETCD_SSL_CERT}" ]; then
	args="${args} --etcd-ssl-cert=${ETCD_SSL_CERT}"
    fi
    if [ -n "${ETCD_SSL_KEY}" ]; then
	args="${args} --etcd-ssl-key=${ETCD_SSL_KEY}"
    fi
    if [ -n "${ETCD_SSL_CA}" ]; then
	args="${args} --etcd-ssl-ca=${ETCD_SSL_CA}"
    fi

    exec /usr/bin/masterelection \
	${args} \
	--master-cmd="/repl_manager.sh master" \
	--slave-cmd="/repl_manager.sh slave"
}

case "$1" in
    master)
	become_master
	;;

    slave)
	if [ -n "${MASTER_ADDR:-}" ]; then
	    become_slave "${MASTER_ADDR}"
	else
	    become_unknown
	fi
	;;

    "")
	sleep 1
	wait_for_mysql

	log "Starting replication manager"
	run_masterelection
	;;

esac

exit 0
