#!/bin/sh

ORIG_ROOT_PASSWORD=mariadb-root
ROOT_PASSWORD=${ROOT_PASSWORD:-${ORIG_ROOT_PASSWORD}}
REPL_PASSWORD=${REPL_PASSWORD:-mariadb-replication}
DATADIR=/data

# Get or generate a unique server ID.
get_server_id() {
    if [ ! -e ${DATADIR}/.server-id ]; then
	# Oops, dash has no $RANDOM.
	shuf -i 1-65535 -n 1 > ${DATADIR}/.server-id
    fi
    cat ${DATADIR}/.server-id
}

# Configure the server, replication state is unknown yet.
configure_server() {
    local server_id=`get_server_id`
    echo "Server ID is ${server_id}" >&2

    # If /data/conf.d exists, load configuration snippets from there.
    if [ -d ${DATADIR}/conf.d ]; then
	echo "!includedir ${DATADIR}/conf.d/" >> /etc/mysql/my.cnf
    fi

    # Write our own custom settings.
    if [ ! -e /etc/mysql/mariadb.conf.d/99-local.cnf ]; then
	cat >/etc/mysql/mariadb.conf.d/99-local.cnf <<EOF
[mysqld]
datadir=${DATADIR}
log_bin=${DATADIR}/binary_log
binlog-format=ROW
bind-address=0.0.0.0
server_id=${server_id}
EOF
    fi
    chmod 644 /etc/mysql/mariadb.conf.d/99-local.cnf

    # Get rid of log_error in the default Debian config, so that
    # mysqld will output errors on stderr.
    sed -e 's/^log_error/#log_error/' -i /etc/mysql/mariadb.conf.d/50-server.cnf
}

# Bootstrap the local MySQL installation.
install_db() {
    if [ ! -d ${DATADIR}/ibdata1 ]; then
	mysql_install_db --user=mysql --datadir=${DATADIR}
    fi
}

# Secure the database.
secure_db() {
    if [ ! -e ${DATADIR}/.initialized ]; then
	# Equivalent of mysql_secure_installation. Set a password for
	# the root user, disable anonymous user and test databases.
	cat <<EOF | mysql
UPDATE mysql.user SET Password=PASSWORD('${ROOT_PASSWORD}') WHERE User='root';
-- Uncomment this to disallow root connections from outside the container.
-- DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
DELETE FROM mysql.user WHERE User='';
DELETE FROM mysql.db WHERE Db='test' OR Db='test_%';
FLUSH PRIVILEGES;

-- Add replication user.
GRANT REPLICATION SLAVE ON *.* TO 'replication'@'%' IDENTIFIED BY '${REPL_PASSWORD}'
EOF
	touch ${DATADIR}/.initialized
    fi

    if [ ! -e /etc/mysql/mariadb.conf.d/99-client.cnf ]; then
        # Write the password in a local config file so clients don't
        # have to remember it. Use UNIX permissions to secure it.
	cat >/etc/mysql/mariadb.conf.d/99-client.cnf <<EOF
[client]
user=root
password=${ROOT_PASSWORD}
EOF
    fi
    chown root.mysql /etc/mysql/mariadb.conf.d/99-client.cnf
    chmod 640 /etc/mysql/mariadb.conf.d/99-client.cnf
}

wait_for_mysql() {
    while ! mysqladmin ping >/dev/null 2>&1; do
	sleep 1
    done
}

# Setup steps before mysqld is started.
setup_pre() {
    mkdir -p ${DATADIR}
    chown mysql.mysql ${DATADIR}
    chmod 700 ${DATADIR}

    mkdir -p /var/run/mysqld
    chown mysql.mysql /var/run/mysqld
    chmod 755 /var/run/mysqld

    configure_server
    install_db
}

# Setup steps after mysqld is started.
setup_post() {
    wait_for_mysql
    secure_db
}


case "$1" in
    pre)
	setup_pre
	;;

    post)
	setup_post
	;;

    *)
	echo "Usage: $0 {pre|post}" >&2
	exit 2
	;;

esac

exit 0
